package SharedObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

public class Balloon extends SharedResource implements Serializable
{
	private static final long serialVersionUID = 1750540724293699377L;
	public short UnitOfWater;
	
	public Balloon() throws NoSuchAlgorithmException, IOException {
		super();
	}
	
	@Override
	protected void AddOwnDataToStream(ByteArrayOutputStream mStream) throws IOException 
	{
		byte[] tmp = BitConverter.getBytes(UnitOfWater);
		mStream.write(tmp, 0, tmp.length);
		super.AddOwnDataToStream(mStream);
	}
}
